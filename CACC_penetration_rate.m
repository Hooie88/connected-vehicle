clc;
%sigmoid parameters
A = 0.4;                       %The lower asymptote
K = 0.8;                       %The upper asymptote
B = 0.001;                      %growth rate
v = 0.03;                      %The length of lower asymptote
total_n = 900;
nn = linspace(1,total_n,total_n);      %The number of vehicles we count

%sigmoid function for driver sensitivity & human driver sensitivity
Al(nn) = A + (K-A)./(1+exp(-B.*nn)).^(1/v);
al = 0.4;

%Time to join a cluster and given parameters
hf = 300;   %freeheadway
vf = 25;    %freeflow speed
hc = 2;     %cluster headway
vc = 2;     %cluster velocity
Hf = 0;     %initial value
Ht = 0;     %initial value
m  = 1;        %initial value



%h_cri = hf.*(power((vc./vf),(1./Al(nn)))); %critical headway
Power_terms = 40;
%Loop for h_cri values
for j=1:1:total_n
    h_cri(j) = hf.*(power((vc./vf),(1./Al(j))));
    if h_cri(j)>2
        vc=0.28; %approximate value..?
        h_cri(j) = hf.*(power((vc./vf),(1./Al(j))));
    end
end
% Loop for K values
for i=1:1:total_n
    K(i)  = vf./(hf.^Al(i));
end
% Loop for h values dependent on different critical headway
for n1=1:1:total_n
    hcri=h_cri(n1);
    for i=1:1:2000
        h = linspace(hf,1.001*hcri,2000); %h_cri term is also based on the number of vehicles
    end
    hvalue(n1,1:2000)=h; % To seperate each h values based on the critical values.
end
% Loop for time to join a cluster
for n=1:1:total_n;
    for i = 1:1:Power_terms
        while (m<=i)
            Hf = Hf + (hf).*((1./(1 - m*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hf).^(m.*Al(n)))); %For joining a cluster
            Ht = Ht + (hvalue(n,1:2000)).*((1./(1 - m.*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hvalue(n,1:2000)).^(m*Al(n))));  %For joining a cluster
            sum = (Hf- Ht);
            m = m + 1;
        end
        t(i,1:length(sum)) = sum;
    end
    T(n,1:40,1:2000)=t; %3 dimension matrix time to join a cluster with respect to size of cluster.
    %To bring back to original values.
    Hf = 0;
    Ht = 0;
    t=0;
    m=1;
end
%Truncation ratio based on the size of cluster
TR = zeros(1,total_n);
Tr = 1.35; % Truncation ratio when driver sensitivity 0.4
for i=1:1:total_n
    TR(i) = T(i,40,2000)/T(i,1,2000);
end

%calculate h_free dependent on the number of vehicle in a cluster and parameters
P = linspace(0,1,11); %CACC proportion rate values 
l = 5;                %length of vehicle
t_leave = 5;        %transition probability rate leaving a cluster
L = 5000;
hf_human = 100;
k=vf/(hf_human^al);
w_ = 1/t_leave;

%Loop for proportion rate of CACC
for p=1:1:11
    
    %Loop for the number of vehicles we count
    for i=1:1:total_n
        %root for transcendental equation solution
        f = @(h_free) (1-P(p))*((k*(1-al)/Tr)*(1/(h_free^(1-al)-hc^(1-al)))) + (P(p))*((K(i)*(1-Al(i))/TR(i))*(1/(h_free^(1-Al(i))-hc^(1-Al(i))))) - w_;
        %root from effective transition probability function  
        h_free(p,i) = fsolve(f,100);             
    end
end

%Loop for penetration rate
for i=1:1:11
    %Loop for normalized values for n_star and rho_star
    for n2=1:1:total_n        
        rho_star(n2) =  (l + (h_free(i,n2)-hc).*(n2.*l./L))/(h_free(i,n2)+l);
        n_star(n2) = n2*l/L;
    end
    effx(i,1:total_n) = rho_star;
    effy(i,1:total_n) = n_star;
    plot(rho_star,n_star)
    xlim([0 0.71])
    ylim([0 0.71])
    xlabel('Rho-star')
    ylabel('n-star')
    hold on;
    grid on;
end
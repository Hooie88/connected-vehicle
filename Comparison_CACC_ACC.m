clc;clear all;

%Sigmoid parameters
A = 0.4;                       %The lower asymptote boundary
K = 0.8;                       %The upper asymptote boundary
B = 0.6;                     %growth rate
v = 0.03;                      %The length of lower asymptote
total_n = 50;                 %The total number of vehicles on a road
nn = linspace(1,total_n,total_n);      %The number of vehicles we count

%Driver sensitivity as sigmoid function.
Al(nn) = A + (K-A)./(1+exp(-B.*nn)).^(1/v);

%Human_driven vehicle's constant driver sensitivity.
al1 = 0.4;
al2 = 0.7;

%Initial parameters to calculate the time taken to join a cluster
hf = 300;   %DSRC channel freeheadway (meter)
vf = 25;    %freeflow speed (meter/second)
hc = 2;     %cluster headway(meter)
vc = 2;     %cluster velocity(meter/second)

Hf = 0;     %initial value
Ht = 0;     %initial value
m  = 1;     %Iteration of hypergeometric terms 
Power_terms = 40;

%Loop for h_cri values
for j=1:1:total_n
    h_cri(j) = hf.*(power((vc./vf),(1./Al(j))));
    if h_cri(j)>2
        vc=0.28; %approximate value..?
        h_cri(j) = hf.*(power((vc./vf),(1./Al(j))));
    end
end
% Loop for K values
for i=1:1:total_n
    K(i)  = vf./(hf.^Al(i));
end
% Loop for h values dependent on different critical headway
for n1=1:1:total_n
    hcri=h_cri(n1);
    for i=1:1:2000
        h = linspace(hf,1.001*hcri,2000); %h_cri term is also based on the number of vehicles
    end
    hvalue(n1,1:2000)=h; % To seperate each h values based on the critical values.
end
% Loop for time to join a cluster
for n=1:1:total_n;
    for i = 1:1:Power_terms
        while (m<=i)
            Hf = Hf + (hf).*((1./(1 - m*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hf).^(m.*Al(n)))); %For joining a cluster
            Ht = Ht + (hvalue(n,1:2000)).*((1./(1 - m.*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hvalue(n,1:2000)).^(m*Al(n))));  %For joining a cluster
            sum = (Hf- Ht);
            m = m + 1;
        end
        t(i,1:length(sum)) = sum;
    end
    T(n,1:40,1:2000)=t; %3 dimension matrix time to join a cluster with respect to size of cluster.
    %To bring back to original values.
    Hf = 0;
    Ht = 0;
    t=0;
    m=1;
end
%Truncation ratio based on the size of cluster
TR = zeros(1,total_n);
Tr = 1.35; % Truncation ratio when driver sensitivity 0.4
for i=1:1:total_n
    TR(i) = T(i,40,2000)/T(i,1,2000);
end
%time to leave the cluster
q_max = 1800;     %flow rate (veh/h) 
VF = 90;          %speed(km/h)
rho_c = q_max/VF; %critical density point
rho = linspace(20,100,900); %density starts from rho_c (N/L)
q_sys1 = 0; %when rho is maximum
w = 6;%(q_max - q_sys1)/(rho_c-rho_max); %wave speed

q_sys = q_max - w*(rho_c - rho);

v_avg = (q_sys./rho)/3.6; %average speed (meter per second)

alpha = 2.94; 
be = 0.0503;
v0 = 0;
t_leave = 5;%(1/be)*(log((alpha-(be.*v0))./(alpha-(be.*v_avg))));

%calculate h_free dependent on the number of vehicle in a cluster and parameters
P = linspace(0,1,11); %CACC proportion rate values 
l = 5;                %length of vehicle
%t_leave = 5;        %transition probability rate leaving a cluster
L = 5000;
hf_human = 100;
k1=vf/(hf_human^al1);
k2=vf/(hf_human^al2);
w_ = 1./t_leave;

%Loop for proportion rate of CACC
for p=1:1:11
    
    %Loop for the number of vehicles we count
    for i=1:1:total_n
        %root for transcendental equation solution
        f1 = @(h_free) (1-P(p))*((k1*(1-al1)/Tr)*(1/(h_free^(1-al1)-hc^(1-al1)))) + (P(p))*((K(i)*(1-Al(i))/TR(i))*(1/(h_free^(1-Al(i))-hc^(1-Al(i))))) - w_;
        f2 = @(h_free) (1-P(p))*((k1*(1-al1)/Tr)*(1/(h_free^(1-al1)-hc^(1-al1)))) + (P(p))*((k2*(1-al2)/Tr)*(1/(h_free^(1-al2)-hc^(1-al2)))) - w_;
        %root from effective transition probability function  
        h_free1(p,i) = fsolve(f1,100);
        h_free2(p,i) = fsolve(f2,100);
    end
end

%Loop for penetration rate
for i=1:2:11
    %Loop for normalized values for n_star and rho_star
    for n2=1:1:total_n        
        rho_star1(n2) =  (l + (h_free1(i,n2)-hc).*(n2.*l./L))/(h_free1(i,n2)+l);
        n_star1(n2) = n2*l/L;
        rho_star2(n2) =  (l + (h_free2(i,n2)-hc).*(n2.*l./L))/(h_free2(i,n2)+l);
        n_star2(n2) = n2*l/L;
    end
    %To figure out where the critical density exits
    norm_den1(i) = rho_star1(1,1);
    effx1(i,1:total_n) = rho_star1;
    effy1(i,1:total_n) = n_star1;
    
    norm_den2(i) = rho_star2(1,1);
    effx1(i,1:total_n) = rho_star2;
    effy1(i,1:total_n) = n_star2;
%     plot(rho_star,n_star)
%     xlim([0 0.9])
%     ylim([0 0.9])
%     xlabel('Rho-star')
%     ylabel('n-star')
%     hold on;
%     grid on;
end
g1 = find(norm_den1 ~= 0);
Norm_den1 = norm_den1(g1);
CACC_pro = linspace(0,1,length(Norm_den1));

g2 = find(norm_den2 ~= 0);
Norm_den2 = norm_den2(g2);
ACC_pro = linspace(0,1,length(Norm_den2));

figure(1)
plot(CACC_pro, Norm_den1, ACC_pro, Norm_den2);
xlabel('Proportion of ACC and CACC vehicles');
ylabel('Normalized critical density');
grid on;



clc;clear all;
%Sigmoid parameters
A = 0.4;                          %The lower asymptote boundary
k = 0.8;                          %The upper asymptote boundary
B = 0.1;                          %Starting point to increase driver sensitivity
v = 0.03;                            %Figuring out initial point on lower asymptote
total_n = 900;                    %The total number of vehicles on a road
nn = linspace(1,total_n,total_n); %The number of vehicles we count
%Opti = 0.001;                     %Optimazing 'v' parameters

%Loop for finding appropriate 'v' value to start Al(1)=0.4;
% while (true)
%         v = v-Opti;
%         Al(1) = A + (k-A)./(1+0.8*exp(-B)).^(1/v);
%         if Al(1)<=A
%             break
%         end
%         i=i+1;
% end

%State-dependent function for driver sensitivity.
Al(nn) = A + (k-A)./(1+0.8*exp(-B.*nn)).^(1/v);
%Sigmoid function
% figure(1)
% plot(nn,Al,'linewidth',2)
% xlabel('The number of vehicles, n')
% ylabel('Driver sensitivity, \alpha(n)')
% xlim([0,total_n])
% ylim([A,1])
% grid on;

%Initial parameters to calculate the time taken to join a cluster
hf = 300;       %DSRC channel freeheadway (meter)
vf = 25;        %freeflow speed (meter/second)
hc = 2;         %cluster headway(meter)
vc = 2;         %cluster velocity(meter/second)
Al_human = 0.4; %Human_driven vehicle driver sensitivity
Al_ACC = 0.7;   %ACC-enabled vehicle driver sensitivity

%Initial condition to calculate truncation ratio
Hf = 0;     %initial value
Ht = 0;     %initial value
m  = 1;     %Iteration of hypergeometric terms 
Power_terms = 40; %Iteration numbers

%Loop for h_cri values and K values
n=1; %n variable is equal to the number of vehicles stuck in a cluster 
while n<=total_n
    h_cri(n) = hf.*(power((vc./vf),(1./Al(n))));
    if h_cri(n)>2
        vc=0.28; %approximate value
        h_cri(n) = hf.*(power((vc./vf),(1./Al(n))));
    end   
    K(n)  = vf./(hf.^Al(n));
    n=n+1;
end

%Loop for deriving h values depending on the state dependent critical headway.
steps=2000; %simulation total number of steps
for n=1:1:total_n
    hcri=h_cri(n);
    for i=1:1:steps
        h = linspace(hf,1.001*hcri,2000); %h_cri term is also based on the number of vehicles
    end
    hvalue(n,1:2000)=h; % To seperate each h values based on the critical values.
end

%Loop for calculating time taken to join a cluster
n=1;       %Initial value of vehicle clusters
Tr = 1.35; %Average truncation ratio if human driver sensitivity 0.4
while n<=total_n; 
    for i = 1:1:Power_terms
        while (m<=i)
            Hf = Hf + (hf).*((1./(1 - m*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hf).^(m.*Al(n)))); %For joining a cluster
            Ht = Ht + (hvalue(n,1:2000)).*((1./(1 - m.*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hvalue(n,1:2000)).^(m*Al(n))));  %For joining a cluster
            sum = (Hf- Ht);
            m = m + 1;
        end
        t(i,1:length(sum)) = sum;
    end
    T(n,1:40,1:2000)=t; %three dimension matrix time to join a cluster with respect to size of cluster.
    TR(n) = T(n,40,steps)/T(n,1,steps); %Truncation ratio dependent on the vehicles cluster.
    %To bring back to original values.
    Hf = 0;Ht = 0;t=0;m=1;
    n=n+1;
end

%Calculate h_free dependent on the number of vehicle in a cluster and parameters
numb=11;                   %This is the number of proportion results. if it increases, it gives more smooth graph but it takes so much time (about 30min when numb is 100).
P = linspace(0,1,numb);    %CACC proportion rate values 
l = 5;                     %Effective length of vehicles
t_leave = 5;               %Time taken to leave a cluster
L = 5000;                  %Total length of ring shaped road.
hf_human = 100;            %human-driven vehicles' free headway
k1=vf/(hf_human^Al_human); %human-driven vehicles' constant value
k2=vf/(hf_human^Al_ACC);   %ACC-enabled vehicles' constant value
w_ = 1./t_leave;           %Transition probability rate of leaving a cluster

%Loop for proportion rate of CACC
for p=1:1:numb  
    %Loop for the number of vehicles we count
    for i=1:1:total_n
        %root for transcendental equation solution
        f1 = @(h_free1) (1-P(p))*((k1*(1-Al_human)/Tr)*(1/(h_free1^(1-Al_human)-hc^(1-Al_human)))) + (P(p))*((K(i)*(1-Al(i))/TR(i))*(1/((h_free1)^(1-Al(i))-hc^(1-Al(i))))) - w_; %CACC and human
        f2 = @(h_free2) (1-P(p))*((k1*(1-Al_human)/Tr)*(1/(h_free2^(1-Al_human)-hc^(1-Al_human)))) + (P(p))*((k2*(1-Al_ACC)/Tr)*(1/(h_free2^(1-Al_ACC)-hc^(1-Al_ACC)))) - w_;   %ACC and human        
        %root from effective transition probability function  
        h_free1(p,i) = fsolve(f1,100);
        h_free2(p,i) = fsolve(f2,100);      
    end
end

%Loop for penetration rate under multi-species environment
for i=1:1:numb
    %Loop for normalized values for n_star and rho_star
    for n2=1:1:total_n        
        rho_star1(n2) =  (l + (h_free1(i,n2)-hc).*(n2.*l./L))/(h_free1(i,n2)+l);
        n_star1(n2) = n2*l/L;
        rho_star2(n2) =  (l + (h_free2(i,n2)-hc).*(n2.*l./L))/(h_free2(i,n2)+l);
        n_star2(n2) = n2*l/L;
    end
    x(i,:) = rho_star1;
    y(i,:) = n_star1;
    %To figure out where the critical density exits
    norm_den1(i) = rho_star1(1,1);  
    norm_den2(i) = rho_star2(1,1);  
%     figure(2)
%     plot(rho_star1,n_star1)
%     xlim([0 0.9])
%     ylim([0 0.9])
%     xlabel('Dimensionless density, \rho^*')
%     ylabel('Normalized cluster size, , \langlen\rangle^{*}')
%     hold on;
%     grid on;
end

%Effective critical density initial parameters
proportion=linspace(0,1,numb);
n1(1)=y(1,1);
n2(1)=y(1,1);
m_min(1)=(y(1,2)-y(1,1))/(x(1,2)-x(1,1));
m_max(1)=(y(1,2)-y(1,1))/(x(1,2)-x(1,1));
effective_cri(1) = x(1,1);
%Loop for slope
for i=2:1:numb
    for j=1:1:length(x)-1
        slope(i,j) = (y(i,j+1)-y(i,j))/(x(i,j+1)-x(i,j));
    end
end
rho=linspace(0,1,length(slope));
%Loop for two lines
for p=2:1:numb

    m_min(p)=min(slope(p,:));
    rho_min(p)=find(m_min(p)>=slope(p,:));
    rho1(p) = x(p,rho_min(p));
    n1(p) = y(p,rho_min(p));
    c1(p) = n1(p)-m_min(p)*rho1(p);
    
    m_max(p)=max(slope(p,:));
    rho_max(p)=find(m_max(p)<=slope(p,:));
    rho2(p) = x(p,rho_max(p));
    n2(p) = y(p,rho_max(p));
    c2(p) = n2(p)-m_max(p)*rho2(p);
end
%Loop for effective critical density
for i=2:1:numb
    N1(i,1:length(rho)) = m_min(i).*rho+c1(i);
    N2(i,1:length(rho)) = m_max(i).*rho+c2(i);
    ff(i)=min(abs(N2(i,1:length(rho))-N1(i,1:length(rho))));
    loco=find(ff(i)>=abs(N2(i,1:length(rho))-N1(i,1:length(rho))));
    effective_cri(i)=rho(loco);
end

%Comparison ACC and CACC 
g1 = find(norm_den1 ~= 0);
Norm_den1 = norm_den1(g1);
CACC_pro = linspace(0,1,length(Norm_den1));
g2 = find(norm_den2 ~= 0);
Norm_den2 = norm_den2(g2);
ACC_pro = linspace(0,1,length(Norm_den2));

figure(3)
plot(proportion,effective_cri,CACC_pro, Norm_den1, ACC_pro, Norm_den2,'linewidth',2)
xlabel('Proportion of CACC and ACC');
ylabel('Normalized critical density');
grid on;
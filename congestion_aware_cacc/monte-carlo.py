
import numpy as np
import matplotlib.pyplot as plt
import warnings
import seaborn as sns


class MonteCarlo:
    """ Class to handle the Monte Carlo simulation for congestion aware CACC code that tests theoretical results """

    def __init__(self):
        """ Initializae the class for a MCMC simulation """
        self.iterations = 1000      # Number of iterations of Monte Carlo simulation
        self.params = dict


class CaccSigmoid:
    """ Class to generate sigmoid that defines CACC behavior and transition probabilities"""
    def __init__(self, **kwargs):
        """ Use input arguments to reassign the default CACC sigmoid parameters"""
        # if **kwargs contains an argument, change argument to that provided, else do nothing
        if 'A' not in kwargs:
            self.A = 0.4  # lower asymptote boundary - lower driver sensitivity
        else:
            self.A = kwargs['A']
        if 'K' not in kwargs:
            self.K = 0.8  # upper asymptote boundary - higher driver sensitivity
        else:
            self.K = kwargs['K']
        if 'B' not in kwargs:
            self.B = 0.6  # sigmoid growth rate
        else:
            self.B = kwargs['B']
        if 'v' not in kwargs:
            self.v = 0.03  # length of lower sigmoid asymptote
        else:
            self.v = kwargs['v']

    def plot_sigmoid(self, num_cars):
        """ Plot the sigmoid using its characteristics"""
        try:
            num_cars        # number of cars in the downstream cluster
        except NameError:
            warnings.warn('Variable num_cars does not exist. Continuing with default value')
        else:
            num_cars = 100
        num_array = np.linspace(1, num_cars, 10*num_cars)

        # Evaluate the CACC sensitivity w.r.t. the number of downstream cluster size
        cacc_sensitivity = self.A + (self.K - self.A)/(np.power((1 + np.exp(-self.B*num_array)), (1/self.v)))

        # Plot the sigmoid
        sns.set_style("whitegrid")

        plt.plot(num_array, cacc_sensitivity, sns.xkcd_rgb["cerulean"], lw=3)
        plt.xlabel('Number of vehicles in cluster', fontsize=16, fontname='Arial')
        plt.ylabel('Driver sensitivity', fontsize=16, fontname='Arial')
        axes = plt.gca()
        axes.set_ylim(0.95*self.A, 1.05*self.K) 
        plt.show()

if __name__ == '__main__':
    mcmc = MonteCarlo()
    # mcmc.initialize()       # Add any arguments if you want to initialize this with custom values

    sigmoid = CaccSigmoid(B=0.3, v=0.001)
    sigmoid.plot_sigmoid(num_cars=100)     # num_cars = number of cars in the downstream cluster


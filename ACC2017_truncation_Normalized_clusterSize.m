clc;clear all;
%%sigmoid parameters
A = 0.4;                       %The lower asymptote
K = 0.9;                       %The upper asymptote
B = 0.6;                       %growth rate
v = 0.01;                      %The length of lower asymptote
nn = linspace(1,900,900);      %The number of vehicles we count
 
%%sigmoid function for driver sensitivity
Al(nn) = A + (K-A)./(1+exp(-B.*nn)).^(1/v); %driver sensitivity we count
al = 0.4;

% figure(1)
% plot(nn, Al, 'linewidth',3)
% ylim([0.4 1])
% xlabel('Number of vehicles in cluster')
% ylabel('Driver sensitivity')
% grid on;
% hold on;

%%Time to join a cluster
hf = 100;   %freeheadway
vf = 25;    %freeflow speed
hc = 2;     %cluster headway
vc = 0.28;  %cluster velocity
Hf = 0;     %initial value
Ht = 0;     %initial value
m=1;        %initial value  
h_cri = hf.*(power((vc./vf),(1./Al(nn)))); %critical headway
Power_terms = 40;
total_n = 900;

% Loop for K values
for i=1:1:total_n
    K(i)  = vf./(hf.^Al(i));
end
% Loop for h values dependent on different critical headway
for n1=1:1:total_n
    hcri=h_cri(n1);
    for i=1:1:2000
        h = linspace(hf,1.001*hcri,2000); %h_cri term is also based on the number of vehicles
    end
    hvalue(n1,1:2000)=h; % To seperate each h values based on the critical values.
end

% % Loop for time to join a cluster
% for n=1:1:total_n;
%     for i = 1:1:Power_terms
%         while (m<=i)
%             Hf = Hf + (hf).*((1./(1 - m*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hf).^(m.*Al(n)))); %For joining a cluster
%             Ht = Ht + (hvalue(n,1:2000)).*((1./(1 - m.*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hvalue(n,1:2000)).^(m*Al(n))));  %For joining a cluster
%             sum = (Hf - Ht);
%             m = m + 1;
%         end
%         
%         t(i,1:length(sum)) = sum;
%     end
%     T(n,1:40,1:2000)=t; %3 dimension matrix time to join a cluster with respect to size of cluster.
%     %To bring back to original values.
%     Hf = 0;
%     Ht = 0;
%     t=0;
%     m=1;
% end

%Truncation ratio based on the size of cluster

% TR = zeros(1,900);
% for i=1:1:900
%     TR(i) = T(i,40,2000)/T(i,1,2000);
% end
TR = 1.7476;
N = 900;
L = 5000;
l = 5;
t_leave = 5;
for N1=1:1:N
    for n=1:1:N1     %expected cluster size 
    h_ss_phy(N1,n) = (L-(N1*l)-(n-1)*hc)/(N1-n+1);     
    h_ss(N1,n)  = (hc^(1-Al(n)) + (t_leave*K(n)*(1-Al(n))/TR))^(1/(1-Al(n))); %steady state headway
    end      
end

c1 = abs((h_ss - h_ss_phy)./h_ss_phy);
c2 = find(abs(c1)==0);
c1(c2)=1;
[row,column] = find(c1<0.01);
%---------------------------------------------------------------------------------------------------
storage = [row,column];

sto=unique(storage,'rows'); %rearrange mixed number as orderly way
g=find(diff(sto(:,1))==0);  %find the starting point to show the same number in sto first column

%c(sto(g(1)),:)=min(c(sto(g(1)),:));
for i=1:1:length(g)
[Row(i),Column(i)]=min(c1(sto(g(i)),:));
column(g(i))=Column(i);
end

row = sort(row);
n = column;
N = row;

rho_star = N*l/L;
n_star = n*l/L;

% figure(2)
% plot(Al,TR,'linewidth',3)
% xlabel('Driver sensitivity')
% ylabel('Truncation ratio, t_{join}/t_1')
% grid on;

figure(3)
plot(rho_star,n_star, 'linewidth',3)
grid on;
xlim([0 0.72])
ylim([0 0.72])
xlabel('Normalized density')
ylabel('Normalized cluster')
hold on;

%rho_critical = l./(h_ss+l);

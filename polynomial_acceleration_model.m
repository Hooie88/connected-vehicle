clc;clear all;
vi=0;
m  = 0.9; 
r = ((1+2*m)^(2+1/m))/(4*m^2);
ta = 10.9;
t = linspace(0,10.9,100);
theta = t./ta;
a_max = 2.69;

Accel = r*a_max*theta.*(1-theta.^m).^2;
Velo  = vi + 3.6*r*a_max*ta.*theta.^2.*(0.5 - 2.*theta.^m/(m+2)+theta.^(2*m)/(2*m+2));
Dist  = (vi.*t/3.6) + (r*a_max*ta^2).*theta.^3.*(1/6 - (2.*theta.^m/((m+2)*(m+3)))...
        + theta.^2*m/((2*m+2)*(2*m+3)));
figure(1)
plot(t,Accel,'linewidth',2)
xlabel('simulation time (sec)')
ylabel('acceleration (m/s^2)')
grid on;
 
figure(2)
plot(t,Velo,'linewidth',2)
xlabel('simulation time (sec)')
ylabel('Velocity (Km/h)')
grid on;

figure(3)
plot(t,Dist,'linewidth',2)
xlabel('simulation time (sec)')
ylabel('Distance (m)')
grid on;
hold on;
clc;
%sigmoid parameters
A = 0.4;                       %The lower asymptote
K = 0.8;                       %The upper asymptote
B = 0.09;                      %growth rate
v = 0.03;                      %The length of lower asymptote
total_n = 100;
nn = linspace(1,total_n,total_n);      %The number of vehicles we count

%sigmoid function for driver sensitivity & human driver sensitivity
Al(nn) = A + (K-A)./(1+exp(-B.*nn)).^(1/v);
al = 0.4;

%Time to join a cluster and given parameters
hf = 300;   %freeheadway
vf = 25;    %freeflow speed
hc = 2;     %cluster headway
vc = 2;     %cluster velocity
Hf = 0;     %initial value
Ht = 0;     %initial value
m=1;        %initial value



%h_cri = hf.*(power((vc./vf),(1./Al(nn)))); %critical headway
Power_terms = 40;
%Loop for h_cri values
for j=1:1:total_n
    h_cri(j) = hf.*(power((vc./vf),(1./Al(j))));
    if h_cri(j)>2
        vc=0.28; %approximate value..?
        h_cri(j) = hf.*(power((vc./vf),(1./Al(j))));
    end
end
% Loop for K values
for i=1:1:total_n
    K(i)  = vf./(hf.^Al(i));
end
% Loop for h values dependent on different critical headway

for n1=1:1:total_n
    hcri=h_cri(n1);
    for i=1:1:2000
        h = linspace(hf,1.001*hcri,2000); %h_cri term is also based on the number of vehicles
    end
    hvalue(n1,1:2000)=h; % To seperate each h values based on the critical values.
end
% Loop for time to join a cluster
for n=1:1:total_n;
    for i = 1:1:Power_terms
        while (m<=i)
            Hf = Hf + (hf).*((1./(1 - m*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hf).^(m.*Al(n)))); %For joining a cluster
            Ht = Ht + (hvalue(n,1:2000)).*((1./(1 - m.*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hvalue(n,1:2000)).^(m*Al(n))));  %For joining a cluster
            sum = (Hf- Ht);
            m = m + 1;
        end
        t(i,1:length(sum)) = sum;
    end
    T(n,1:40,1:2000)=t; %3 dimension matrix time to join a cluster with respect to size of cluster.
    %To bring back to original values.
    Hf = 0;
    Ht = 0;
    t=0;
    m=1;
end
%Truncation ratio based on the size of cluster
TR = zeros(1,total_n);
Tr = 1.35; % Truncation ratio when driver sensitivity 0.4
for i=1:1:total_n
    TR(i) = T(i,40,2000)/T(i,1,2000);
end

for n=1:1:total_n
    t1(n,1:2000) = (hf.^(1-Al(n))-hvalue(n,1:2000).^(1-Al(n)))./(K(n).*(1-Al(n)));
end

i=[1,43,100];
plot(TR(i(1)).*t1(i(1),1:2000),hvalue(i(1),1:2000),TR(i(3)).*t1(i(3),1:2000),hvalue(i(3),1:2000),'linewidth',3)
title('headway versus time')
xlabel('simulation tiem(sec)')
ylabel('headway(meter)')
grid on;



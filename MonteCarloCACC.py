import random
import numpy
import math
import matplotlib.pyplot as plt
# parameters for traffic description
L = 5000            # length of road
l = 5               # effective length of vehicle
vc = hc = float(1)  # cluster velocity, #cluster headway
hf = 100            # free headway
chf = 300           # connected vehicle free headway
vf = 25             # free flow velocity
human = 0.4         # human driver sensitivity
Tr = 1.35           # truncation ratio
kh = vf / (hf ** human)     # state dependent
t_leave = 5         # transition probability rate of leaving a cluster

# size of clusters for beginning
rho = float(input("Select traffic densities between 0.1 to 0.7 "))
N = rho*L/l
n = numpy.linspace(1, N, N)


# Function of sigmoid driver sensitivity provided by parameters
def sigmoid():
    # Given four different parameters
    A = 0.4
    K = 0.8
    B = 0.06
    v = 0.03
    Al = [0]*n      # To allocate spaces
    for i in range(len(n)):
        Al[i] = A + (K-A)/(1+math.exp(-B*n[i]))**(1/v)
    return Al

CACC = sigmoid()    # assign sigmoid function result

# Function of state dependent function
kc = [0]*n


def kcFun():
    k=0
    for k in range(len(n)):
        kc[k] = vf / (chf ** CACC[k])
    return kc
Kc = kcFun()

# Loop for proportion rate of human and CACC
P = numpy.linspace(0,1,11)
wminus = float(1)/t_leave
h = [0]*n
wplus = numpy.zeros((len(P),len(n)))
Wplus = numpy.zeros((len(P),len(n)))
w_eff = numpy.zeros((len(P),len(n)))
Ti = float(1)
for p in range(11):
    i=0
    # Loop for transition probability rate of joining a cluster
    while i<=len(n)-1:
        h[i] = (L - l*N - (n[i]-1)*hc)/(N - n[i] + 1)
        wplus[p][i] = kh*(1-human)/Tr*(1/(h[i]**(1-human)-hc**(1-human)))
        Wplus[p][i] = Kc[i]*(float(1)-CACC[i])/Ti*(float(1)/(h[i]**(float(1)-CACC[i])-hc**(float(1)-CACC[i])))
        w_eff[p][i] = (float(1)-P[p])*wplus[p][i] + P[p]*Wplus[p][i]
        i=i+1


# To bound maximum probability of transition
find=numpy.where(w_eff>1)
w_eff[find]=1

# Monte-Carlo simulation and Random cluster size
monte_steps = 10000;
clustersize = [0]*monte_steps
clustersize[0] = round(N*random.random())
normalized_clustersize=numpy.zeros((len(P),len(clustersize)))

# Initial condition on random transition rate of joining or leaving a cluster
randomIN = [0]*monte_steps
randomOUT = [0]*monte_steps
randomIN[0] = random.random()
randomOUT[0] = random.random()


def monte_simul():
    for p in range(11):
        time = 0
        for time in range(monte_steps-1):
            # To except the errors,
            if clustersize[time]==0:
                clustersize[time]=1

            # To simulate monte-carlo simulation, give condition between transition rates from given and random values
            if randomIN[time]<w_eff[p][int(clustersize[time])] and randomOUT[time]<wminus:
                clustersize[time+1]=clustersize[time]
            elif randomIN[time]>w_eff[p][int(clustersize[time])] and randomOUT[time]>wminus:
                clustersize[time+1]=clustersize[time]
            elif randomIN[time]>w_eff[p][int(clustersize[time])] and randomOUT[time]<wminus:
                clustersize[time+1]=clustersize[time]-1
            elif randomIN[time]<w_eff[p][int(clustersize[time])] and randomOUT[time]>wminus:
                clustersize[time+1]=clustersize[time]+1

            # Input next random values for both join and leave
            randomIN[time+1] = random.random()
            randomOUT[time+1] = random.random()   
    # Assign data space of Normalized clustersize
        
    # Loop for allocating each clustersize value
        for assign in range(len(clustersize)):
            normalized_clustersize[p][assign]=clustersize[assign]*l/L
            # To derive the result of Normalized_clustersize computation.
    return normalized_clustersize

# Store monteCarlo simulation result
NormalizedClustersize = monte_simul()

# Plot Normalized cluster size (proportion 0 means 0% of CACC and 10 means 100% of CACC)
InterestedProportion = int(input("Select proportion rate of CACC between 0 and 10 "))
plt.plot(NormalizedClustersize[InterestedProportion][:])
plt.ylim([0, 1])
plt.grid()
plt.show()


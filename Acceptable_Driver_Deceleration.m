clc; clear all;

Al = [0.4 0.5 0.8 1.2];% driver sensitivity
hf = 300;         % Free headway
vf = 25;          % Free velocity
vref = 0.25;         % Cluster/target velocity
endTime  = 100;
incrTime = 0.001; % Time step for numerical integration
t = linspace(0,endTime,endTime/incrTime);

h(1) = hf;        % initial headway
v(1) = vf;        % initial velocity
b = -3.4*ones(size(t));

%Loop for driver sensitivity values
for m=1:1:length(Al)
    %Loop for deceleration values
    for i=1:1:length(t)-1
        a(i+1)   = (Al(m)*v(i)/h(i))*(vref - v(i)); % GM fourth model
        hOld     = h(i);
        vOld     = v(i);
        aOld     = a(i+1);
        h(i+1)   = hOld + incrTime*(vref - vOld);
        v(i+1)   = vOld + incrTime*aOld;
    end
    plot(t,a,t,b,'linewidth',1)
    xlabel('simulation time(sec)')
    ylabel('Deceleration(m/s^2)')
    grid on;
    hold on;

end




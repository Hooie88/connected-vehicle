clc;clear all;
%Sigmoid parameters
A = 0.4;                          %The lower asymptote boundary
K = 0.8;                          %The upper asymptote boundary
B = 0.6;                          %Starting point to increase driver sensitivity
v = B;                            %Figuring out initial point on lower asymptote
total_n = 100;                    %The total number of vehicles on a road
nn = linspace(1,total_n,total_n); %The number of vehicles we count


Opti = 0.001; %Optimazing 'v' parameters
%Loop for finding appropriate 'v' value to start Al(1)=0.4;
while (true)
        v = v-Opti;
        Al(1) = A + (K-A)./(1+0.8*exp(-B)).^(1/v);
        if Al(1)<=A
            break
        end
        i=i+1;
end
%Driver sensitivity as sigmoid function.
Al(nn) = A + (K-A)./(1+0.8*exp(-B.*nn)).^(1/v);
figure(1)
plot(nn,Al,'linewidth',3)
xlabel('The number of vehicles')
ylabel('Driver sensitivity')
xlim([0,100])
ylim([A,1])
grid on;
hold on;

al = 0.4; %human driver sensitivity

%%Time to join a cluster
hf = 300;   %freeheadway
vf = 25;    %freeflow speed
hc = 2;     %cluster headway
vc = 0.28;  %cluster velocity
Hf = 0;     %initial value
Ht = 0;     %initial value
m=1;        %initial value  
h_cri = hf.*(power((vc./vf),(1./Al(nn)))); %critical headway
Power_terms = 40;


% Loop for K values
for i=1:1:total_n
    K(i)  = vf./(hf.^Al(i));
end
% Loop for h values dependent on different critical headway
for n1=1:1:total_n
    hcri=h_cri(n1);
    for i=1:1:2000
        h = linspace(hf,1.001*hcri,2000); %h_cri term is also based on the number of vehicles
    end
    hvalue(n1,1:2000)=h; % To seperate each h values based on the critical values.
end

% Loop for time to join a cluster
for n=1:1:total_n
    for i = 1:1:Power_terms
        while (m<=i)
            Hf = Hf + (hf).*((1./(1 - m*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hf).^(m.*Al(n)))); %For joining a cluster
            Ht = Ht + (hvalue(n,1:2000)).*((1./(1 - m.*Al(n))).*((vc.^(m-1))./((vf).^m)).*((hf./hvalue(n,1:2000)).^(m*Al(n))));  %For joining a cluster
            sum = (Hf - Ht);
            m = m + 1;
        end
        
        t(i,1:length(sum)) = sum;
    end
    T(n,1:40,1:2000)=t; %3 dimension matrix time to join a cluster with respect to size of cluster.
    %To bring back to original values.
    Hf = 0;
    Ht = 0;
    t=0;
    m=1;
end

%Truncation ratio based on the size of cluster
TR = zeros(1,total_n);
for i=1:1:total_n
    TR(i) = T(i,40,2000)/T(i,1,2000);
end
for n=1:1:total_n
    t1(n,1:2000) = (hf.^(1-Al(n))-hvalue(n,1:2000).^(1-Al(n)))./(K(n).*(1-Al(n)));
end
i=[1,43,100];
figure(2)
plot(TR(i(1)).*t1(i(1),1:2000),hvalue(i(1),1:2000),TR(i(2)).*t1(i(2),1:2000),hvalue(i(2),1:2000),'linewidth',3)
title('headway versus time')
xlabel('simulation tiem(sec)')
ylabel('headway(meter)')
grid on;
hold on;

% figure(2)
% plot(Al,TR,'linewidth',3)
% xlabel('Driver sensitivity')
% ylabel('Truncation ratio, t_{join}/t_1')
% grid on;


clc;clear all;
%% analytical equation
q_max = 1800; %flow rate (veh/h) 
vf = 90;      %speed(km/h)
rho_c = q_max/vf; %critical density point
rho = linspace(0,100,100); %density starts from rho_c (N/L)
q_sys1 = 0; %when rho is maximum
w = 6;%(q_max - q_sys1)/(rho_c-rho_max); %wave speed

for(i=1:1:length(rho))
    % For free flow side of fundamental diagram
    if(rho(i) < rho_c)
        q_sys(i) = vf*rho(i);
    % For congested side of fundamental diagram
    else
        q_sys(i) = q_max - w*(rho_c - rho(i));
    end
end

v_avg = (q_sys./rho)/3.6; %average speed (meter per second)

%From Gary long acceleration characteristics
% al = 2.94 and be = 0.0503 is first entry in Table 2
al = 2.94; %1.82; %because of Gary long acceleration equation, we can't use this value resulting in complex number.
be = 0.0503;
v0 = 0;
t = (1/be)*(log((al-(be.*v0))./(al-(be.*v_avg))));
figure(1)
plot(rho,t)
xlabel('density (veh/km)')
ylabel('time taken to leave dependent on density')

grid on

grid on;
%% Gary long equation
clc; clear all;
v_max = 25;
q_max = 1800; %flow rate (veh/h) 
vf = 90;      %speed(km/h)
rho_c = q_max/vf; %critical density point
rho = linspace(20,100,1000); %density starts from rho_c (N/L)
q_sys1 = 0; %when rho is maximum
w = 6;%(q_max - q_sys1)/(rho_c-rho_max); %wave speed

q_sys = q_max - w*(rho_c - rho);

v_avg = (q_sys./rho)/3.6; %average speed (meter per second)

%From Gary long acceleration characteristics
% al = 2.94 and be = 0.0503 is first entry in Table 2
al = 2.02; %1.82; %because of Gary long acceleration equation, we can't use this value resulting in complex number.
be = 0.12;
v0 = 0;
T = (1/be)*(log((al-(be.*v0))./(al-(be.*v_avg)))); 

%gary long equation
t_val = linspace(0,100,100);
v = al/be - (al/be - v0)*exp(-be*t_val);
d = al/be*t_val - (al/be - v0)*(1- exp(-be*t_val))/be;
figure(2)
plot(rho, T)
xlabel('density')
ylabel('time taken to leave dependent on density')
grid on;
figure(3)
plot(t_val,d)
xlabel('time(sec)')
ylabel('speed(m/s)')
grid on;



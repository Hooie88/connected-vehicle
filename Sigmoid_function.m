clc;clear all;
%sigmoid parameters
A = 0.4;                       %The lower asymptote
K = 0.9;                       %The upper asymptote
B = 0.1;                       %growth rate
v = 0.01;                      %The length of lower asymptote
nn = linspace(1,100,100);      %The number of vehicles we count

%sigmoid function for driver sensitivity & human driver sensitivity
Al(nn) = A + (K-A)./(1+exp(-B.*nn)).^(1/v);
plot(Al)
xlabel('The number of vehicle')
ylabel('Driver sensitivity')
